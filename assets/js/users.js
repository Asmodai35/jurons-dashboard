
$(document).on("click", ".btn-activate", function() {
    let userId = $(this).data("user-id");
    $.ajax({
        url: `/users/${userId}/activate`,
        method: 'POST',
        success : function(result) {
            if(result.state === 'OK') {
                $('#flash-messages').html('<div class="alert alert-success">'+result.message+'</div>')
                $('#activate-user-'+userId).hide();
                $('#deactivate-user-'+userId).show();
            } else {
                $('#flash-messages').html('<div class="alert alert-danger">'+result+'</div>')
            }
        }
    })
})
$(document).on("click", ".btn-deactivate", function() {
    let userId = $(this).data("user-id");
    $.ajax({
        url: `/users/${userId}/deactivate`,
        method: 'POST',
        success : function(result) {
            if(result.state === 'OK') {
                $('#flash-messages').html('<div class="alert alert-success">'+result.message+'</div>')
                $('#deactivate-user-'+userId).hide();
                $('#activate-user-'+userId).show();
            } else {
                $('#flash-messages').html('<div class="alert alert-danger">'+result+'</div>')
            }
        }
    })
})

$(function() {
    $('#users-table thead tr').clone(true).attr('id', 'filter-users').appendTo( '#users-table thead' );
    let table = $('#users-table').DataTable({
        order: [[ 0, "asc" ]],
        orderCellsTop: true,
        //fixedHeader: true,
        paging: false,
        dom: "itr",
        language: {
            info: "Affichage des utilisateurs _START_ à _END_ sur _TOTAL_",
            infoEmpty: "Il n'y a aucun utilisateur",
            infoFiltered:  "(filtrés depuis _MAX_ utilisateurs)",
            thousands: " ",
            decimal: ",",
            sZeroRecords: "Pas de résultat pour ce(s) filtre(s)"
        },
        columns: [
            { width: "15%"},
            { width: "15%"},
            { width: "10%"},
            { width: "25%"},
            { width: "10%"},
            { width: "10%"},
            { width: "15%", orderable: false },
        ]
    });
    $('#filter-users th').each( function (i) {
        var title = $(this).text();

        if (i < $(document).find('#filter-users th').length - 1) {
            if (i == 2) {
                let html = '<select class="form-control"><option value="">'+title+'</option>';
                table.column(i).data().unique().sort().each( function ( d, j ) {
                    html += '<option value="'+d+'">'+d+'</option>';
                } );
                html+='</select>';
                $(this).html(html);
            }
            else {
                $(this).html( '<input type="text" class="form-control" placeholder="Rechercher par '+title+'"/>' );
            }
        } else {
            $(this).empty();
        }
        $('input', this).on('keyup change', function () {
            if (table.column(i).search() !== this.value) {
                table
                    .column(i)
                    .search(this.value)
                    .draw();
            }
        } );
        $('select', this).on('change', function () {
            if (table.column(i).search() !== this.value) {

                table
                    .column(i)
                    .search(this.value)
                    .draw();
            }
        });
    } );
})