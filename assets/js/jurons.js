$(document).on("click", ".btn-edit", function() {
    scrollTo(0,0);
    let id = $(this).data("id");
    $('#edit-form form').attr('action', `/swearword/${id}/edit`);
    $('#edit-form input#swearword_label').val($(this).data('label'));
    $('#edit-form input#swearword_price').val($(this).data('price'));
    $('#form').hide();
    $('#add-btn').show();
    $('#edit-form').show();
})

$(function() {
    $('#jurons-table thead tr').clone(true).attr('id', 'filter-jurons').appendTo( '#jurons-table thead' );
    let table = $('#jurons-table').DataTable({
        order: [[ 1, "asc" ]],
        orderCellsTop: true,
        //fixedHeader: true,
        paging: false,
        dom: "itr",
        language: {
            info: "Affichage des jurons _START_ à _END_ sur _TOTAL_",
            infoEmpty: "Il n'y a aucun jurons",
            infoFiltered:  "(filtrés depuis _MAX_ jurons)",
            thousands: " ",
            decimal: ",",
            sZeroRecords: "Pas de résultat pour ce(s) filtre(s)"
        },
        columns: [
            { width: "59%"},
            { width: "10%"},
            { width: "10%"},
            { width: "11%"},
            { width: "10%", orderable: false },
        ]
    });
    $('#filter-jurons th').each( function (i) {
        var title = $(this).text();

        if (i < $(document).find('#filter-jurons th').length - 1) {
            $(this).html( '<input type="text" class="form-control" placeholder="Rechercher par '+title+'"/>' );
        } else {
            $(this).empty();
        }
        $('input', this).on('keyup change', function () {
            if (table.column(i).search() !== this.value) {
                table
                    .column(i)
                    .search(this.value)
                    .draw();
            }
        } );
        $('select', this).on('change', function () {
            if (table.column(i).search() !== this.value) {

                table
                    .column(i)
                    .search(this.value)
                    .draw();
            }
        });
    } );
});