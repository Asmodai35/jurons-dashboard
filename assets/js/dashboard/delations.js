const months = {
    "1": "Janvier",
    "2": "Février",
    "3": "Mars",
    "4": "Avril",
    "5": "Mai",
    "6": "Juin",
    "7": "Juillet",
    "8": "Août",
    "9": "Septembre",
    "10": "Octobre",
    "11": "Novembre",
    "12": "Décembre",
}
$(function() {
    $.ajax({
        url: "/dashboard/delations",
        type: "POST",
        success: setYearChart
    })
    $.ajax({
        url: "/dashboard/delations/month",
        type: "POST",
        success: setMonthChart
    })
})

$(document).on("click", "#hide-year", function() {
    $(this).hide();
    $('#show-year').show();
    $('#year-body').fadeOut();
});
$(document).on("click", "#show-year", function() {
    $(this).hide();
    $('#hide-year').show();
    $('#year-body').fadeIn();
});
$(document).on("click", ".btn-year", function() {
    let forYear = $(this).data('year');
    $.ajax({
        url: "/dashboard/delations",
        type: "POST",
        data: {year: forYear},
        success: setYearChart
    })
})
function setYearChart(result) {
    $('.btn-year').removeClass('disabled');
    $(`#btn-year-${result.year}`).addClass("disabled");
    let resultDatas = new Array();
    for (let user of result.users) {
        let userTable = result.delations[user.username];
        let color = typeof(user.color) != "undefined" ? user.color : "#f8f8f8";
        let userData = {
            data: Object.values(userTable),
            label: user.username,
            backgroundColor: color  + "60",
            borderColor: color,
            borderWidth: 1
        }
        resultDatas.push(userData);
    }
    const datas = {
        labels: Object.values(months),
        datasets: resultDatas
    }
    const config = {
        type: 'bar',
        data: datas,
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        },
    };
    if (result.sumReports != 0) {
        $('#chart-year-container').html("<canvas id='chart-year'></canvas>");
        new Chart(
            document.getElementById('chart-year'), 
            config
        );
    } else {
        $('#chart-year-container').html("<h5>Pas de données</h5>");
    }
}

$(document).on("click", "#hide-month", function() {
    $(this).hide();
    $('#show-month').show();
    $('#month-body').fadeOut();
});
$(document).on("click", "#show-month", function() {
    $(this).hide();
    $('#hide-month').show();
    $('#month-body').fadeIn();
});
$(document).on("click", ".btn-month", function() {
    let forMonth = $(this).data('month');
    $.ajax({
        url: "/dashboard/delations/month",
        type: "POST",
        data: {month: forMonth},
        success: setMonthChart
    })
})
function setMonthChart(result) {
    $('.btn-month').removeClass('disabled');
    $(`#btn-month-${result.month}`).addClass("disabled");
    let users = new Array(),
    colors = new Array();
    for (let user of Object.values(result.users)) {
        users.push(user.username);
        colors.push(user.color)
    }
    const datas = {
        labels: users,
        datasets: [{
            data: Object.values(result.delations),
            label: "datas",
            backgroundColor: colors,
            hoverOffset: 4
        }]
    }
    const config = {
        type: 'pie',
        data: datas
    };
    if (result.sumReports != 0) {
        $('#chart-month-container').html("<canvas id='chart-month'></canvas>");
        new Chart(
            document.getElementById('chart-month'), 
            config
        );
    } else {
        $('#chart-month-container').html("<h5>Pas de données</h5>");
    }
}