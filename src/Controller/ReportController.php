<?php


namespace App\Controller;


use App\Entity\Balance;
use App\Entity\Report;
use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ReportController extends AbstractController
{
    /**
     * @Route("/reports", name="all_reports")
     */
    public function index() {
        $allReports = $this->getDoctrine()->getRepository(Report::class)
            ->findBy([], ['dateCrea' => 'DESC']);

        $reports = [];
        $year = $allReports[0]->getDateCrea()->format('Y');
        $month = $allReports[0]->getDateCrea()->format('F');
        $reports[$year] = [];
        $reports[$year][$month] = [];
        foreach($allReports as $report) {
            if($year == $report->getDateCrea()->format('Y')) {
                if($month == $report->getDateCrea()->format('F')) {
                    $reports[$year][$month][] = $report;
                } else {
                    // On change de mois
                    $reports[$year][$month] = array_reverse($reports[$year][$month]);
                    $month = $report->getDateCrea()->format('F');
                    $reports[$year][$month] = [];
                    $reports[$year][$month][] = $report;
                }
            } else {
                // On change d'année
                $reports[$year][$month] = array_reverse($reports[$year][$month]);
                $year = $report->getDateCrea()->format('Y');
                $month = $report->getDateCrea()->format('F');
                $reports[$year][$month] = [];
                $reports[$year][$month][] = $report;
            }
        }
        $reports[$year][$month] = array_reverse($reports[$year][$month]);

        return $this->render('report/index.html.twig', [
            'controller_name' => 'ReportController',
            'reports' => $reports
        ]);
    }

    /**
     * @Route("/report/validations", name="report_validations")
     */
    public function validations() {
        $reportsToValid = $this->getDoctrine()->getRepository(Report::class)
            ->findBy(['valid' => '0'], ['dateCrea' => 'ASC']);

        if (empty($reportsToValid)) {
            return $this->redirectToRoute('homepage');
        }
        return $this->render('report/validations.html.twig', [
            'controller_name' => 'ReportController',
            'reports' => $reportsToValid
        ]);
    }

    /**
     * @Route("/report/{id}/validate", name="report_validate", requirements={"id"="\d+"})
     */
    public function validate($id) {
        $reportToValid = $this->getDoctrine()->getRepository(Report::class)->find($id);

        if($reportToValid->getUserCrea() == $this->getUser() && $reportToValid->getUser() != $this->getUser()) {
            return new JsonResponse('you cannot do this');
        }

        $reportToValid->setValid(true);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse('report validated');
    }


    /**
     * @Route("/report/{id}/refuse", name="report_refuse", requirements={"id"="\d+"})
     */
    public function refuse($id) {
        $reportToValid = $this->getDoctrine()->getRepository(Report::class)->find($id);

        if($reportToValid->getUser() == $this->getUser() && $reportToValid->getUserCrea() != $this->getUser()) {
            return new JsonResponse('you cannot do this');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($reportToValid);
        $em->flush();

        return new JsonResponse('report refused');
    }

    /**
     * @Route("/report/payments", name="report_payments")
     */
    public function payments() {
        $reportsToPay = $this->getDoctrine()->getManager()->getRepository(Report::class)->findPayments();

        if (empty($reportsToPay)) {
            return $this->redirectToRoute('homepage');
        }
        return $this->render('report/payments.html.twig', [
            'controller_name' => 'ReportController',
            'reports' => $reportsToPay
        ]);
    }

    /**
     * @Route("/report/{userId}/pay", name="report_pay", requirements={"userId"="\d+"})
     */
    public function pay($userId) {
        $user = $this->getDoctrine()->getRepository(User::class)->find($userId);


        if($user == $this->getUser()) {
            return new JsonResponse('you cannot do this');
        }

        $em = $this->getDoctrine()->getManager();
        $reportsToPay = $this->getDoctrine()->getManager()->getRepository(Report::class)->findPayments();
        $items = $this->getDoctrine()->getManager()->getRepository(Balance::class)->findAll();
        $lastBalance = $items ? $items[count($items) - 1]: null;
        $reportsPaid = $this->getDoctrine()->getRepository(Report::class)->findBy(['user' => $user]);
        foreach ($reportsPaid as $report) {
            $report->setPaid(true);
        }
        foreach($reportsToPay as $reportToPay) {
            if($reportToPay['userId'] == $userId) {
                $balance = new Balance();
                $balance->setLabel('Paiement de '.$reportToPay['user']);
                $balance->setCredit($reportToPay['price']);
                $balance->setInitialAmount($lastBalance ? $lastBalance->getRestAmount(): 0);
                $balance->setRestAmount($balance->getInitialAmount() + $balance->getCredit());
                $em->persist($balance);
                break;
            }
        }
        $em->flush();

        return new JsonResponse('user paid');
    }
}