<?php


namespace App\Controller;


use App\Entity\Report;
use App\Entity\User;
use App\Form\Type\UserType;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/users", name="users")
     */
    public function index() {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        $user = new User();

        $form = $this->createForm(UserType::class, $user);

        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
            'users' =>$users,
            'form' =>$form->createView()
        ]);
    }

    /**
     * @Route("/users/{id}", name="user_view", requirements={"id"="\d+"})
     */
    public function view(int $id){
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $records = $this->getDoctrine()->getRepository(Report::class)
            ->findBy(['user'=>$user, 'valid' => '1'], ['dateCrea'=>'DESC']);
        $reports = $this->getDoctrine()->getRepository(Report::class)
            ->findBy(['userCrea'=>$user, 'valid' => '1'], ['dateCrea'=>'DESC']);

        return $this->render('user/view.html.twig', [
            'controller_name' => 'UserController',
            'user' => $user,
            'records' => $records,
            'reports' => $reports
        ]);
    }

    /**
     * @Route("/users/add", name="user_add")
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        $user = new User();

        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            try {
                $user->setPassword($passwordEncoder->encodePassword(
                    $user,
                    'test'
                ));
                $em->persist($user);
                $em->flush();
                $this->addFlash('success', 'User has been created');

                //TODO sendMail
            } catch (Exception $e) {
                $this->addFlash('error', $e->getMessage());
            }

            return $this->redirectToRoute('users');

        }

        return $this->render('user/add.html.twig', [
            'controller_name' => 'UserController',
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/users/{id}/edit", name="user_edit", requirements={"id"="\d+"})
     */
    public function edit(int $id, Request $request) {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            try{
                $em->flush();
                $this->addFlash('success', 'User has been updated');
            } catch(Exception $e){
                $this->addFlash('error', $e->getMessage());
            }

            // redirects to the "homepage" route
            return $this->redirectToRoute('users');
        }
        return $this->render('user/edit.html.twig', [
            'controller_name' => 'UserController',
            'form' => $form->createView(),
            'userId' => $id
        ]);

    }

    /**
     * @Route("/users/{id}/deactivate", name="user_deactivate", requirements={"id"="\d+"})
     */
    public function deactivate(int $id, TranslatorInterface $translator) {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $user->setActive(false);
        $em = $this->getDoctrine()->getManager();
        try{
            $em->flush();
        } catch(Exception $e){
            return new JsonResponse($e->getMessage());
        }


        return new JsonResponse(['state' => 'OK', 'message' => $translator->trans('User has been deactivated')]);

    }

    /**
     * @Route("/users/{id}/activate", name="user_activate", requirements={"id"="\d+"})
     */
    public function activate(int $id, TranslatorInterface $translator) {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $user->setActive(true);
        $em = $this->getDoctrine()->getManager();
        try{
            $em->flush();
        } catch(Exception $e){
            return new JsonResponse($e->getMessage());
        }

        return new JsonResponse(['state' => 'OK', 'message' => $translator->trans('User has been activated')]);
    }
}