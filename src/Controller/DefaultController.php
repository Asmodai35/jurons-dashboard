<?php

namespace App\Controller;

use App\Entity\Balance;
use App\Entity\User;
use App\Form\Type\ReportType;
use App\Entity\Report;
use App\Repository\ReportRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_login');
        }

        $reportRepo = $this->getDoctrine()->getManager()->getRepository(Report::class);
        $userRepo = $this->getDoctrine()->getManager()->getRepository(User::class);
        $balanceRepo = $this->getDoctrine()->getRepository(Balance::class);

        $lastReports = $reportRepo->findBy(['valid' => '1'], ['dateCrea'=> 'desc'], 5);
        $dispoAmount = $balanceRepo->getLatest() ? $balanceRepo->getLatest()->getRestAmount() : 0;
        $unpaidAmount = $reportRepo->getTotalUnpaid() ? $reportRepo->getTotalUnpaid() : ['total' => 0];
        $cagnotte = $dispoAmount + $unpaidAmount['total'];
        $totalToPay = $reportRepo->getTotalForUser($this->getUser());
        $pronounced = $reportRepo->getThisMonth();
        $myRudnessTotal = $reportRepo->getCostForUser($this->getUser());
        $mostRude = $reportRepo->getMostRude();
        $mostCollabo = $reportRepo->getMostCollabo();
        if ($mostRude) {
            if ($mostRude['user'] == $this->getUser()->getFirstname() . ' ' . $this->getUser()->getName()) {
                $mostRude = "Me";
            } else {
                if ($mostRude['surname']) {
                    $mostRude = $mostRude['surname'];
                } else {
                    $mostRude = $mostRude['user'];
                }
            }
        }
        if ($mostCollabo) {
            if ($mostCollabo['user'] == $this->getUser()->getFirstname() . ' ' . $this->getUser()->getName()) {
                $mostCollabo = "Me";
            } else {
                if ($mostCollabo['surname']) {
                    $mostCollabo = $mostCollabo['surname'];
                } else {
                    $mostCollabo = $mostCollabo['user'];
                }
            }
        }

        $allUsers = $userRepo->findAll();
        $birthdays = [];
        $ausydays = [];
        $todayDate = new \DateTime('now');
        foreach ($allUsers as $user) {
            if($user->getBirthday() && $user->getBirthday()->format('m-d') == $todayDate->format('m-d')) {
                $birthdays[] = $user;
            }
            if($user->getAusyArrivalDate() && $user->getAusyArrivalDate()->format('m-d') == $todayDate->format('m-d')) {
                $ausydays[] = $user;
            }
        }

        $toValidate = !empty($reportRepo->findBy(['valid' => '0']));
        $toPay = !empty($reportRepo->findBy(['valid' => '1', 'paid' => '0']));

        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'lastReports' => $lastReports,
            'cagnotte' => $cagnotte,
            'dispoAmount' => $dispoAmount,
            'totalToPay' => $totalToPay['total'],
            'pronounced' => $pronounced['total'],
            'toValidate' => $toValidate,
            'toPay' => $toPay,
            'myRudnessTotal' => $myRudnessTotal,
            'mostRude' => $mostRude,
            'mostCollabo' => $mostCollabo,
            'birthdays' => $birthdays,
            'ausydays' => $ausydays
        ]);
    }

    /**
     * @Route("/delations-table", name="delations_table")
     */
    public function delationsTable(ReportRepository $reportRepo, UserRepository $userRepo)
    {
        $delations = array();
        $users = $userRepo->findBy(['active' => true], ["firstname" => "ASC"]);
        foreach ($users as $user) {
            $delation = array();
            $delation["swearwordsForMonth"] = 0;
            $delation["swearwords"] = 0;
            $delation["user"] = $user->getFirstname() . "&nbsp;";
            if ($user->getSurname()) {
                $delation["user"] .= "<b>&laquo;&nbsp;" . $user->getSurname() . "&nbsp;&raquo;</b>&nbsp;";
            }
            $delation["user"] .= $user->getName();
            $delationsByUser = $reportRepo->findDelationsForUser($user);

            $delationsThisMonth = $reportRepo->getDelationsForUserThisMonth($user);
            foreach ($delationsByUser as $delationByUser) {
                $delation["swearwords"] = $delationByUser['swearwords'];
                if ($delationsThisMonth) {
                    foreach ($delationsThisMonth as $delationThisMonth) {
                        if ($delationThisMonth['user'] == $delationByUser['user']) {
                            $delation["swearwordsForMonth"] = $delationThisMonth["swearwordsThisMonth"];
                            break;
                        }
                    }
                }
            }
            $delations[] = $delation;
        }
        return $this->render('default/delations_table.html.twig', [
            'delationsByUser' => $delations
        ]);
    }

    /**
     * @Route("/reports-table", name="reports_table")
     */
    public function reportsTable(ReportRepository $reportRepo, UserRepository $userRepo)
    {
        $reports = array();
        $users = $userRepo->findBy(['active' => true], ["firstname" => "ASC"]);
        foreach ($users as $user) {
            $report = array();
            $report["swearwordsForMonth"] = 0;
            $report["swearwords"] = 0;
            $report["user"] = $user->getFirstname() . "&nbsp;";
            if ($user->getSurname()) {
                $report["user"] .= "<b>&laquo;&nbsp;" . $user->getSurname() . "&nbsp;&raquo;</b>&nbsp;";
            }
            $report["user"] .= $user->getName();
            $reportsByUser = $reportRepo->findForUser($user);

            $reportsThisMonth = $reportRepo->getForUserThisMonth($user);
            foreach ($reportsByUser as $reportByUser) {
                $report["swearwords"] = $reportByUser['swearwords'];
                if ($reportsThisMonth) {
                    foreach ($reportsThisMonth as $reportThisMonth) {
                        if ($reportThisMonth['user'] == $reportByUser['user']) {
                            $report["swearwordsForMonth"] = $reportThisMonth["swearwordsThisMonth"];
                            break;
                        }
                    }
                }
            }
            $reports[] = $report;
        }

        return $this->render('default/reports_table.html.twig', [
            'reportsByUser' => $reports
        ]);
    }

    /**
     * @Route("/report", name="report")
     */
    public function report(Request $request, TranslatorInterface $translator)
    {
        $report = new Report();
        $report->setDateCrea(new \DateTime());

        $user = $this->getUser();
        $form = $this->createForm(ReportType::class, $report);

        $form->add('submit', SubmitType::class, array(
            'label' => $translator->trans('Report').' !',
            'attr'=>['class'=>'btn btn-success']
        ));


        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            try {
                $report->setUserCrea($user);
                if($report->getUser()->getAusyArrivalDate()->format('m-d') == (new \DateTime())->format('m-d')) {
                    $report->setPrice($report->getSwearword()->getPrice() / 2);
                } else {
                    $report->setPrice($report->getSwearword()->getPrice());
                }
                $em->persist($report);
                $em->flush();
                $this->addFlash('success', 'Report has been created');
            } catch (Exception $e) {
                $this->addFlash('error', $e->getMessage());
            }

            return $this->redirectToRoute('homepage');
        }
        return $this->render('default/report.html.twig', [
            'controller_name' => 'DefaultController',
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/rules", name="rules", methods={"GET"})
     */
    public function rules()
    {
        return $this->render("default/rules.html.twig");
    }
}
