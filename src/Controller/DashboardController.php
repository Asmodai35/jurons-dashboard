<?php

namespace App\Controller;

use App\Repository\ReportRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/dashboard")
 */
class DashboardController extends AbstractController
{
    /**
     * @Route("/pronounciations", name="dashboard_pronounciations", methods= {"GET", "POST"})
     */
    public function pronounciations(Request $request, ReportRepository $reportRepo, UserRepository $userRepo)
    {
        if ($request->getMethod() == "POST") {
            $users = array();
            $allUsers = $userRepo->findBy(['active' => true], ["firstname" => "ASC"]);
            $year = $request->get('year') ? $request->get('year') : (new \Datetime())->format('Y');
            $sumReports = 0;
            foreach ($allUsers as $user) {
                $username = $user->getFirstname() . " " . $user->getName();
                $users[] = [
                    "username" => $username,
                    "color" => $user->getColor()
                ];
                $reports[$username] = array();
                $reports[$username]["1"] = $reports[$username]["2"] = $reports[$username]["3"]
                    = $reports[$username]["4"] = $reports[$username]["5"] = $reports[$username]["6"]
                    = $reports[$username]["7"] = $reports[$username]["8"] = $reports[$username]["9"]
                    = $reports[$username]["10"] = $reports[$username]["11"] = $reports[$username]["12"] = "0";
    
                $reportsThisMonth = $reportRepo->getReportsForYear($year, $user);
                if ($reportsThisMonth) {
                    foreach ($reportsThisMonth as $reportThisMonth) {
                        $reports[$username][$reportThisMonth["mois"]] = $reportThisMonth["total"];
                        $sumReports += $reportThisMonth["total"];
                    }
                }
            }

            return new JsonResponse([
                "reports" => $reports,
                "users" => $users,
                "sumsReports" => $sumReports,
                "year" => $year
            ]);
        }
        $years = $reportRepo->findYears();

        return $this->render('dashboard/pronounciations.html.twig', [
            'years' => $years,
        ]);
    }

    /**
     * @Route("/pronounciations/month", name="dashboard_pronounciations_month", methods= {"POST"})
     */
    public function pronounciationsMonth(Request $request, ReportRepository $reportRepo, UserRepository $userRepo)
    {
        $users = array();
        $month = $request->get('month') ? $request->get('month') : (new \Datetime())->format('m');
        $allUsers = $userRepo->findBy(['active' => true], ["firstname" => "ASC"]);
        $sumReports = 0;
        foreach ($allUsers as $user) {
            $username = $user->getFirstname() . " " . $user->getName();
            $users[] = [
                "username" => $username,
                "color" => $user->getColor()
            ];
            $reports[$username] = "0";

            $reportsThisMonth = $reportRepo->getReportsForMonth($month, $user);
            if ($reportsThisMonth) {
                foreach ($reportsThisMonth as $reportThisMonth) {
                    $reports[$username] = $reportThisMonth["total"];
                    $sumReports += $reportThisMonth["total"];
                }
            }
        }

        return new JsonResponse([
            "reports" => $reports,
            "users" => $users,
            "month" => $month,
            "sumReports" => $sumReports
        ]);
    }

    /**
     * @Route("/delations", name="dashboard_delations", methods= {"GET", "POST"})
     */
    public function delations(Request $request, ReportRepository $reportRepo, UserRepository $userRepo)
    {
        if ($request->getMethod() == "POST") {
            $users = array();
            $year = $request->get('year') ? $request->get('year') : (new \Datetime())->format('Y');
            $allUsers = $userRepo->findBy(['active' => true], ["firstname" => "ASC"]);
            $sumReports = 0;
            foreach ($allUsers as $user) {
                $username = $user->getFirstname() . " " . $user->getName();
                $users[] = [
                    "username" => $username,
                    "color" => $user->getColor()
                ];
                $delations[$username] = array();
                $delations[$username]["1"] = $delations[$username]["2"] = $delations[$username]["3"]
                    = $delations[$username]["4"] = $delations[$username]["5"] = $delations[$username]["6"]
                    = $delations[$username]["7"] = $delations[$username]["8"] = $delations[$username]["9"]
                    = $delations[$username]["10"] = $delations[$username]["11"] = $delations[$username]["12"] = "0";
    
                $delationsThisMonth = $reportRepo->getDelationsForYear($year, $user);
                if ($delationsThisMonth) {
                    foreach ($delationsThisMonth as $delationThisMonth) {
                        $delations[$username][$delationThisMonth["mois"]] = $delationThisMonth["total"];
                        $sumReports += $delationThisMonth["total"];
                    }
                }
            }

            return new JsonResponse([
                "delations" => $delations,
                "users" => $users,
                "year" => $year,
                "sumReports" => $sumReports
            ]);
        }
        $years = $reportRepo->findYears();

        return $this->render('dashboard/delations.html.twig', [
            'years' => $years
        ]);
    }

    /**
     * @Route("/delations/month", name="dashboard_delations_month", methods= {"POST"})
     */
    public function delationsMonth(Request $request, ReportRepository $reportRepo, UserRepository $userRepo)
    {
        $users = array();
        $month = $request->get('month') ? $request->get('month') : (new \Datetime())->format('m');
        $allUsers = $userRepo->findBy(['active' => true], ["firstname" => "ASC"]);
        $sumReports = 0;
        foreach ($allUsers as $user) {
            $username = $user->getFirstname() . " " . $user->getName();
            $users[] = [
                "username" => $username,
                "color" => $user->getColor()
            ];
            $delations[$username] = "0";

            $delationsThisMonth = $reportRepo->getDelationsForMonth($month, $user);
            if ($delationsThisMonth) {
                foreach ($delationsThisMonth as $delationThisMonth) {
                    $delations[$username] = $delationThisMonth["total"];
                    $sumReports += $delationThisMonth["total"];
                }
            }
        }

        return new JsonResponse([
            "delations" => $delations,
            "users" => $users,
            "month" => $month,
            "sumReports" => $sumReports
        ]);
    }
}
