<?php

namespace App\Controller;

use App\Entity\Swearword;
use App\Form\Type\SwearwordType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SwearwordController extends AbstractController
{
    /**
     * @Route("/swearword", name="swearword")
     */
    public function index()
    {
        $items = $this->getDoctrine()->getRepository(Swearword::class)->findAll();

        $form = $this->createForm(SwearwordType::class);

        return $this->render('swearword/index.html.twig', [
            'controller_name' => 'SwearwordController',
            'items' => $items,
            'form' => $form->createView(),
            'editForm' => $form->createView()
        ]);
    }
    
    /**
     * @Route("/swearword/add", name="swearword_add")
     */
    public function new(Request $request){
        $swearword = new Swearword();

        $form = $this->createForm(SwearwordType::class, $swearword);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $existing = $this->getDoctrine()->getRepository(Swearword::class)->labelLikes($swearword->getLabel());
            if(!$existing) {
                $em = $this->getDoctrine()->getManager();
                try {
                    $em->persist($swearword);
                    $em->flush();
                    $this->addFlash('success', 'Swearword has been created');
                } catch (Exception $e) {
                    $this->addFlash('error', $e->getMessage());
                }
            } else {
                $this->addFlash('error', 'Swearword already exists');
            }

            // redirects to the "homepage" route
            return $this->redirectToRoute('swearword');
        }

    }


    /**
     * @Route("/swearword/{id}/edit", name="swearword_edit")
     */
    public function edit(int $id, Request $request) {
        $swearword = $this->getDoctrine()->getRepository(Swearword::class)->find($id);

        $form = $this->createForm(SwearwordType::class, $swearword);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            try{
                $em->flush();
                $this->addFlash('success', 'Swearword has been updated');
            } catch(Exception $e){
                $this->addFlash('error', $e->getMessage());
            }

            // redirects to the "homepage" route
            return $this->redirectToRoute('swearword');
        }
    }
}
