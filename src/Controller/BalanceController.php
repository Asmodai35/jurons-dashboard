<?php


namespace App\Controller;


use App\Entity\Balance;
use App\Form\Type\BalanceType;
use App\Repository\BalanceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BalanceController extends AbstractController
{
    /**
     * @Route("/balance", name="balance")
     */
    public function index() {
        $balance = new Balance();
        $form = $this->createForm(BalanceType::class, $balance);
        $editForm = $this->createForm(BalanceType::class, $balance);
        $items = $this->getDoctrine()->getManager()->getRepository(Balance::class)->findAll();
        $currentAmount = 0.00;
        if(count($items) > 0) {
            $currentAmount = $items[count($items) - 1]->getRestAmount();
        }
        $prevAmount = 0.00;
        if(count($items) > 1) {
            $prevAmount = $items[count($items) - 2]->getRestAmount();
        }
        return $this->render('balance/index.html.twig', [
            'form' => $form->createView(),
            'editForm' =>$editForm->createView(),
            'items' => $items,
            'currentAmount' => $currentAmount,
            'prevAmount' => $prevAmount
        ]);
    }

    /**
     * @Route("/balance/new", name="create_balance")
     */
    public function create(Request $request, BalanceRepository $balanceRepository) {
        $balance = new Balance();
        $latest = $balanceRepository->getLatest();
        if($latest == null) {
            $balance->setInitialAmount(0.00);
        } else {
            $balance->setInitialAmount($latest->getRestAmount());
        }
        $form = $this->createForm(BalanceType::class, $balance);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            if($request->request->get('balance')['type'] == 'credit') {
                $balance->setCredit($request->request->get('balance')['amount']);
                $balance->setRestAmount($balance->getInitialAmount() + $balance->getCredit());
            }
            if($request->request->get('balance')['type'] == 'debit') {
                $balance->setDebit($request->request->get('balance')['amount']);
                $balance->setRestAmount($balance->getInitialAmount() - $balance->getDebit());
            }
            $em = $this->getDoctrine()->getManager();
            try {
                $em->persist($balance);
                $em->flush();
                $this->addFlash('success', 'Balance line has been created');
            } catch (Exception $e) {
                $this->addFlash('error', $e->getMessage());
            }
        }
        return $this->redirectToRoute('balance');

    }

    /**
     * @Route("/balance/{id}/edit", name="edit_balance", requirements={"id" = "\d+"})
     */
    public function edit($id, Request $request) {
        $balance = $this->getDoctrine()->getRepository(Balance::class)->find($id);
        $form = $this->createForm(BalanceType::class, $balance);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            if($balance->getInitialAmount() == null) {
                $balance->setInitialAmount(0.00);
            }
            if($request->request->get('balance')['type'] == 'credit') {
                $balance->setCredit($request->request->get('balance')['amount']);
                $balance->setRestAmount($balance->getInitialAmount() + $balance->getCredit());
            }
            if($request->request->get('balance')['type'] == 'debit') {
                $balance->setDebit($request->request->get('balance')['amount']);
                $balance->setRestAmount($balance->getInitialAmount() - $balance->getDebit());
            }
            $em = $this->getDoctrine()->getManager();
            try {
                $em->flush();
                $this->addFlash('success', 'Balance line has been updated');
            } catch (Exception $e) {
                $this->addFlash('error', $e->getMessage());
            }
        }
        return $this->redirectToRoute('balance');
    }

    /**
     * @Route("/balance/{id}/delete", name="delete_balance", requirements={"id" = "\d+"})
     */
    public function delete($id) {
        $balance = $this->getDoctrine()->getRepository(Balance::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        try {
            $em->remove($balance);
            $em->flush();
            $this->addFlash('success', 'Balance line has been removed');
        } catch (Exception $e) {
            $this->addFlash('error', $e->getMessage());
        }
        return $this->redirectToRoute('balance');
    }
}