<?php


namespace App\Controller;

use App\Entity\Report;
use App\Form\Type\ProfileType;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProfileController extends AbstractController
{
    private $token;
    private $passwordEncoder;
    private $currentUser;

    public function __construct(TokenStorageInterface $tokenStorage, UserPasswordEncoderInterface $passwordEncoder) {
        $this->token = $tokenStorage;
        $this->passwordEncoder = $passwordEncoder;
        $this->currentUser = $this->token->getToken()->getUser();
    }

    /**
     * @Route("/profile", name="profile")
     */
    public function view() {
        $records = $this->getDoctrine()->getRepository(Report::class)
            ->findBy(['user'=>$this->currentUser, 'valid' => '1'], ['dateCrea'=>'DESC'], 10);
        $reports = $this->getDoctrine()->getRepository(Report::class)
            ->findBy(['userCrea'=>$this->currentUser, 'valid' => '1'], ['dateCrea'=>'DESC'], 10);

        return $this->render('profile/view.html.twig', [
            'controller_name' => 'ProfileController',
            'user' => $this->currentUser,
            'reports' => $reports,
            'records' => $records
        ]);
    }

    /**
     * @Route("/profile/edit", name="edit_profile")
     */
    public function edit(Request $request) {
        $form = $this->createForm(ProfileType::class, $this->currentUser);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            try {
                $em->persist($this->currentUser);
                $em->flush();
                $this->addFlash('success', 'Your profile has been updated');
            } catch (Exception $e) {
                $this->addFlash('error', $e->getMessage());
            }
            return $this->redirectToRoute('profile');
        }
        return $this->render('profile/edit.html.twig', [
            'controller_name' => 'ProfileController',
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/change_password", name="change_password")
     */
    public function changePassword(Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        $old_pwd = $request->get('old_password');
        $new_pwd = $request->get('new_password');
        $new_pwd_confirm = $request->get('new_password_confirm');
        if (isset($old_pwd)) {
            $checkPass = $passwordEncoder->isPasswordValid($this->currentUser, $old_pwd);
            if ($checkPass === true) {
                if ($new_pwd == $new_pwd_confirm) {
                    $em = $this->getDoctrine()->getManager();
                    try {
                        $this->currentUser->setPassword($passwordEncoder->encodePassword($this->currentUser, $new_pwd_confirm));
                        $em->persist($this->currentUser);
                        $em->flush();

                        $this->addFlash('success', 'Your password has been changed');
                    } catch (Exception $e) {
                        $this->addFlash('error', $e->getMessage());
                    }

                    return $this->redirectToRoute('profile');
                } else {
                    $this->addFlash('error', 'New password and confirm are not the same');
                }
            } else {
                $this->addFlash('error', 'Old password is wrong');
            }
        }
        return $this->render('profile/change_password.html.twig', [
            'controller_name' => 'ProfileController'
        ]);
    }

    /**
     * @Route("profile/reports", name="reports")
     */
    public function reports() {
        $reports = $this->getDoctrine()->getRepository(Report::class)
            ->findBy(['userCrea'=>$this->currentUser], ['dateCrea'=>'DESC']);
        return $this->render('profile/reports.html.twig', [
            'controller_name' => 'ProfileController',
            'reports'=>$reports,
        ]);
    }

    /**
     * @Route("profile/records", name="records")
     */
    public function records() {
        $records = $this->getDoctrine()->getRepository(Report::class)
            ->findBy(['user'=>$this->currentUser, 'valid'=>'true'], ['dateCrea'=>'DESC']);
        return $this->render('profile/records.html.twig', [
            'controller_name' => 'ProfileController',
            'records' => $records,
        ]);
    }
}