<?php


namespace App\Form\Type;


use App\Entity\User;
use DateTime;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, array('label' => 'Email address', 'required' => false, 'disabled' =>true))
            ->add('name', TextType::class, array('label' => 'Name', 'required' => true))
            ->add('firstname', TextType::class, array('label' => 'Firstname', 'required' => true))
            ->add('birthday', DateType::class, array(
                'label' => 'Birthday',
                'required' => false,
                'years'=> range(1950,(new DateTime('now'))->format('Y')),
                'widget' => 'single_text'
            ))
            ->add('surname', TextType::class, array(
                'label' => 'Surnom',
                'required' => false,
            ))
            ->add('ausyArrivalDate', DateType::class, array(
                'label' => 'Ausy Arrival Date',
                'required' => false,
                'years'=> range(1950,(new DateTime('now'))->format('Y')),
                'widget' => 'single_text',
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}