<?php

namespace App\Form\Type;

use App\Entity\Balance;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BalanceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('initial_amount', NumberType::class, array(
                'required' => false,
                'disabled' => true,
                'label' => 'Current amount'
            ))
            ->add('type', ChoiceType::class, array(
                'label' => 'Type',
                'choices' => [
                    'Credit' => 'credit',
                    'Debit' => 'debit'
                ],
                'mapped' => false
            ))
            ->add('amount', NumberType::class, array(
                'required' => true,
                'label' => 'Amount',
                'mapped' => false
            ))
            ->add('label', TextType::class, array(
                'required' => true,
                'label' => 'Label'
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Balance::class,
            'allow_extra_fields' => true
        ]);
    }
}
