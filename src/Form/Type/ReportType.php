<?php


namespace App\Form\Type;


use App\Entity\Report;
use App\Entity\Swearword;
use App\Entity\User;
use App\Repository\SwearwordRepository;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('swearword', EntityType::class, array(
                'class' => Swearword::class,
                'query_builder' => function (SwearwordRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.label', 'ASC');
                },
                'choice_label' => 'label',
                'label' => 'Swearword',
                'placeholder'=>'Choose a swearword',
                'required' => true)
            )
            ->add('swearwordPrice', EntityType::class, array(
                'class' => Swearword::class,
                'query_builder' => function (SwearwordRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.label', 'ASC');
                },
                'choice_label' => 'price',
                'label' => 'Price',
                'placeholder'=>'Choose a swearword',
                'attr' => ['readonly' => true, 'disabled' => true],
                'required' => true,
                'mapped' => false   
            ))
            ->add('user', EntityType::class, array(
                'class' => User::class,
                'query_builder' => function (UserRepository $er) {
                    $date = new \DateTime();
                    return $er->createQueryBuilder('u')
                        ->where('u.active = 1')
                        ->andWhere('MONTH(u.birthday) != :month OR DAY(u.birthday) != :day')
                        ->setParameter('day', $date->format('d'))
                        ->setParameter('month', $date->format('m'))
                        ->orderBy('u.firstname', 'ASC');
                },
                'choice_label' => function($user) {
                    return $user->displayName();
                },
                'label' => 'User',
                'placeholder'=>'Choose a user',
                'required' => true)
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Report::class,
        ]);
    }
}