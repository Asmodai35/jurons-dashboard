<?php


namespace App\Form\Type;

use App\Entity\User;
use DateTime;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, array('label' => 'Email address', 'required' => true))
            ->add('name', TextType::class, array('label' => 'Name', 'required' => true))
            ->add('firstname', TextType::class, array('label' => 'Firstname', 'required' => true))
            ->add('birthday', DateType::class, array(
                'label' => 'Birthday',
                'years'=> range(1950,(new DateTime('now'))->format('Y')),
                'widget' => 'single_text'
            ))
            ->add('surname', TextType::class, array(
                'label' => 'Surnom',
                'required' => false,
            ))
            ->add('ausyArrivalDate', DateType::class, array(
                'label' => 'Ausy Arrival Date',
                'required' => true,
                'years'=> range(1980,(new DateTime('now'))->format('Y')),
                'widget' => 'single_text',
            ))
            ->add('roles', ChoiceType::class, array(
                'choices' => array(
                    'Utilisateur' => 'ROLE_USER',
                    'Modérateur' => 'ROLE_MODERATOR',
                    'Administrateur' => 'ROLE_ADMIN',
                ),
                'label' => 'Role',
                'required' => true,
                'expanded' => true,
                'multiple' => false,
                'attr' => ['class' => 'form-inline justify-content-between border-0 form-control bg-light']
            ))
            ->add('color', TextType::class, array(
                'label' => "Color",
                'required' => false
            ))
        ;
        $builder->get('roles')
            ->addModelTransformer(new CallbackTransformer(
                function ($rolesArray) {
                    // transform the array to a string
                    return count($rolesArray)? $rolesArray[0]: null;
                },
                function ($rolesString) {
                    // transform the string back to an array
                    return [$rolesString];
                }
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}