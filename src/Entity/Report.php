<?php


namespace App\Entity;

use App\Repository\ReportRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Report
 *
 * @ORM\Table(name="report")
 * @ORM\Entity(repositoryClass=ReportRepository::class)
 */
class Report
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Swearword")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="swearword_id", referencedColumnName="id")
     * })
     */
    private $swearword;

    /**
     * @ORM\Column(name="dateCrea", type="datetime", nullable=false)
     */
    private $dateCrea;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     *   @ORM\JoinColumn(name="user_crea", referencedColumnName="id")
     */
    private $userCrea;

    /**
     * @ORM\Column(type="boolean")
     */
    private $valid;

    /**
     * @ORM\Column(type="boolean")
     */
    private $paid;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $price;

    public function __construct() {
        $this->paid = false;
        $this->valid = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setSwearword(Swearword $swearword)
    {
        $this->swearword = $swearword;
    }

    public function getSwearword()
    {
        return $this->swearword;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function setDateCrea($dateCrea)
    {
        $this->dateCrea = $dateCrea;
    }
    public function getDateCrea()
    {
        return $this->dateCrea;
    }

    public function setUserCrea(UserInterface $userCrea)
    {
        $this->userCrea = $userCrea;
    }

    public function getUserCrea()
    {
        return $this->userCrea;
    }

    public function getValid(): ?bool
    {
        return $this->valid;
    }

    public function setValid(bool $valid): self
    {
        $this->valid = $valid;

        return $this;
    }

    public function getPaid(): ?bool
    {
        return $this->paid;
    }

    public function setPaid(bool $paid): self
    {
        $this->paid = $paid;

        return $this;
    }
}