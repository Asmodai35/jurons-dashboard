<?php

namespace App\Entity;

use App\Repository\BalanceRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BalanceRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Balance
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $credit;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $debit;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $initialAmount;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $restAmount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(name="created_at",type="string", length=255)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at",type="string", length=255)
     */
    private $updated_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCredit(): ?string
    {
        return $this->credit;
    }

    public function setCredit(?string $credit): self
    {
        $this->credit = $credit;

        return $this;
    }

    public function getDebit(): ?string
    {
        return $this->debit;
    }

    public function setDebit(?string $debit): self
    {
        $this->debit = $debit;

        return $this;
    }

    public function getInitialAmount(): ?string
    {
        return $this->initialAmount;
    }

    public function setInitialAmount(string $initialAmount): self
    {
        $this->initialAmount = $initialAmount;

        return $this;
    }

    public function getRestAmount(): ?string
    {
        return $this->restAmount;
    }

    public function setRestAmount(string $restAmount): self
    {
        $this->restAmount = $restAmount;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getCreatedAt() {
        return $this->created_at;
    }

    public function setCreatedAt(DateTime $date) {
        $this->created_at = $date->format('d/m/Y');
        return $this;
    }

    public function getUpdatedAt() {
        return $this->updated_at;
    }

    public function setUpdatedAt(DateTime $date) {
        $this->updated_at = $date->format('d/m/Y');
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function createdTimestamps(): void
    {
        $this->setCreatedAt(new DateTime('now'));
        $this->setUpdatedAt(new DateTime('now'));
    }

    /**
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new DateTime('now'));
    }
}
