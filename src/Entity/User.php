<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Error;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Repository\UserRepository;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $firstname;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $birthday;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $ausyArrivalDate;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $surname;

    /**
     * *@ORM\Column(name="created_at",type="string", length=255)
     */
    private $created_at;

    /**
     * *@ORM\Column(name="updated_at",type="string", length=255)
     */
    private $updated_at;

    public function __construct() {
        $this->setRoles(['ROLE_USER']);
    }

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $color;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getDisplayRole() {
        $mainRole = $this->getRoles()[0];
        switch($mainRole) {
            case 'ROLE_ADMIN' :
                return 'Administrateur';
            case 'ROLE_MODERATOR' :
                return 'Modérateur';
            case 'ROLE_USER' :
                return 'Utilisateur';
            default :
                throw new Error('User must have a role');
        }
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function setBirthday(?DateTime $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getAusyArrivalDate()
    {
        return $this->ausyArrivalDate;
    }

    public function setAusyArrivalDate(?DateTime $ausyArrivalDate): self
    {
        $this->ausyArrivalDate = $ausyArrivalDate;

        return $this;
    }

    public function getCreatedAt(){
        return $this->created_at;
    }

    public function setCreatedAt(DateTime $date){
        $this->created_at = $date->format('Y-m-d h:i:s');

        return $this;
    }

    public function getUpdatedAt(){
        return $this->updated_at;
    }

    public function setUpdatedAt(DateTime $date){
        $this->updated_at = $date->format('Y-m-d h:i:s');

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function displayName() {
        return $this->getFirstname() . ' ' . strtoupper($this->getName());
    }

    /**
     * @ORM\PrePersist
     */
    public function createdTimestamps(): void
    {
        $this->setActive(true);
        $this->setCreatedAt(new DateTime('now'));
        $this->setUpdatedAt(new DateTime('now'));
    }

    /**
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new DateTime('now'));
    }

    /**
     * @return mixed
     */
    public function getSurname(){
        return $this->surname;
    }

    /**
     * @param $surname
     *
     * @return $this
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }
}
