<?php

namespace App\Entity;

use App\Repository\SwearwordRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SwearwordRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Swearword
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     */
    private $label;

    /** 
    * *@ORM\Column(name="created_at",type="string", length=255)
    */
    private $created_at;
        
    /** 
    * *@ORM\Column(name="updated_at",type="string", length=255)
    */
    private $updated_at;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $price;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCreatedAt(){
        return $this->created_at;
    }

    public function setCreatedAt(DateTime $date){
        $this->created_at = $date->format('d/m/Y');
        return $this;
    }

    public function getUpdatedAt(){
        return $this->updated_at;
    }

    public function setUpdatedAt(DateTime $date){
        $this->updated_at = $date->format('d/m/Y');
        return $this;
    }

    /**
     * @ORM\PrePersist 
     */
    public function createdTimestamps(): void
    {
        $this->setCreatedAt(new DateTime('now'));
        $this->setUpdatedAt(new DateTime('now'));
    }

    /**
     * @ORM\PreUpdate
    */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new DateTime('now'));
    }
}
