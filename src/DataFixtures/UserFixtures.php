<?php

namespace App\DataFixtures;

use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setName('Auvray');
        $user->setFirstName('Laura');
        $user->setEmail('laura.auvray@ausy.fr');
        $user->setRoles(['ROLE_ADMIN']);
        $user->setBirthday(new DateTime('now'));
        $user->setJoiningDate(new DateTime('now'));
        $user->setActive(true);
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'test'
        ));

        $manager->persist($user);
        $user = new User();
        $user->setName('Audo');
        $user->setFirstName('Fabien');
        $user->setEmail('fabien.audo@ausy.fr');
        $user->setRoles(['ROLE_ADMIN']);
        $user->setActive(true);
        $user->setBirthday(new DateTime('now'));
        $user->setJoiningDate(new DateTime('now'));
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'test'
        ));

        $manager->persist($user);
        $user = new User();
        $user->setName('Tosarelli');
        $user->setFirstName('Tony');
        $user->setEmail('tony.tosarelli@ausy.fr');
        $user->setRoles(['ROLE_USER']);
        $user->setActive(true);
        $user->setBirthday(new DateTime('now'));
        $user->setJoiningDate(new DateTime('now'));
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'test'
        ));

        $manager->persist($user);
        $manager->flush();
    }
}
