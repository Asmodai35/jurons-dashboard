<?php


namespace App\Repository;


use App\Entity\Report;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr\Join;
use Error;

/**
 * @method Report|null find($id, $lockMode = null, $lockVersion = null)
 * @method Report|null findOneBy(array $criteria, array $orderBy = null)
 * @method Report[]    findAll()
 * @method Report[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Report::class);
    }

    public function findByUsers() {
        $qb = $this->createQueryBuilder('r');
        return $qb->select("count(r.swearword) as swearwords, concat(u.firstname, ' ', u.name) as user, u.surname as surname, u.firstname, u.name")
            ->leftJoin('r.user', 'u', Join::WITH, $qb->expr()->eq('u', 'r.user'))
            ->where($qb->expr()->eq('r.valid', 'true'))
            ->groupBy('r.user')
            ->orderBy('swearwords', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function getByUsersThisMonth() {
        $month = intval((new DateTime('now'))->format('m'));
        $year = intval((new DateTime('now'))->format('Y'));
        $startDate = (new DateTime())
            ->setDate($year, $month, 1)
            ->setTime(0,0,0)
            ->format('Y-m-d H:i:s');
        $endDate = (new DateTime())
            ->setDate($year, $month, $this->getEndMonth($month, $year))
            ->setTime(23,59,59)
            ->format('Y-m-d H:i:s');
        $qb = $this->createQueryBuilder('r');
        return $qb->select("count(r.swearword) as swearwordsThisMonth, concat(u.firstname, ' ', u.name) as user, u.surname as surname, u.firstname, u.name")
            ->leftJoin('r.user', 'u', Join::WITH, $qb->expr()->eq('u', 'r.user'))
            ->where('r.dateCrea >= :start_month')
            ->andWhere('r.dateCrea <= :end_month')
            ->andWhere('r.valid = true')
            ->setParameter(':start_month', $startDate)
            ->setParameter(':end_month', $endDate)
            ->groupBy('r.user')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findForUser($user) {
        $qb = $this->createQueryBuilder('r');
        return $qb->select("count(r.swearword) as swearwords, u.id as user")
            ->leftJoin('r.user', 'u', Join::WITH, $qb->expr()->eq('u', 'r.user'))
            ->where($qb->expr()->eq('r.valid', 'true'))
            ->andWhere("r.user = :user")
            ->setParameter("user", $user)
            ->groupBy('r.user')
            ->orderBy('swearwords', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function getForUserThisMonth($user) {
        $month = intval((new DateTime('now'))->format('m'));
        $year = intval((new DateTime('now'))->format('Y'));
        $startDate = (new DateTime())
            ->setDate($year, $month, 1)
            ->setTime(0,0,0)
            ->format('Y-m-d H:i:s');
        $endDate = (new DateTime())
            ->setDate($year, $month, $this->getEndMonth($month, $year))
            ->setTime(23,59,59)
            ->format('Y-m-d H:i:s');
        $qb = $this->createQueryBuilder('r');
        return $qb->select("count(r.swearword) as swearwordsThisMonth, u.id as user")
            ->leftJoin('r.user', 'u', Join::WITH, $qb->expr()->eq('u', 'r.user'))
            ->where('r.dateCrea >= :start_month')
            ->andWhere('r.dateCrea <= :end_month')
            ->andWhere('r.valid = true')
            ->setParameter(':start_month', $startDate)
            ->setParameter(':end_month', $endDate)
            ->andWhere("r.user = :user")
            ->setParameter("user", $user)
            ->groupBy('r.user')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findDelationsForUser($user) {
        $qb = $this->createQueryBuilder('r');
        return $qb->select("count(r.swearword) as swearwords, u.id as user")
            ->leftJoin('r.userCrea', 'u', Join::WITH, $qb->expr()->eq('u', 'r.userCrea'))
            ->where($qb->expr()->eq('r.valid', 'true'))
            ->andWhere("r.userCrea = :user")
            ->setParameter("user", $user)
            ->groupBy('r.userCrea')
            ->orderBy('swearwords', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function getDelationsForUserThisMonth($user) {
        $month = intval((new DateTime('now'))->format('m'));
        $year = intval((new DateTime('now'))->format('Y'));
        $startDate = (new DateTime())
            ->setDate($year, $month, 1)
            ->setTime(0,0,0)
            ->format('Y-m-d H:i:s');
        $endDate = (new DateTime())
            ->setDate($year, $month, $this->getEndMonth($month, $year))
            ->setTime(23,59,59)
            ->format('Y-m-d H:i:s');
        $qb = $this->createQueryBuilder('r');
        return $qb->select("count(r.swearword) as swearwordsThisMonth, u.id as user")
            ->leftJoin('r.userCrea', 'u', Join::WITH, $qb->expr()->eq('u', 'r.userCrea'))
            ->where('r.dateCrea >= :start_month')
            ->andWhere('r.dateCrea <= :end_month')
            ->andWhere('r.valid = true')
            ->setParameter(':start_month', $startDate)
            ->setParameter(':end_month', $endDate)
            ->andWhere("r.userCrea = :user")
            ->setParameter("user", $user)
            ->groupBy('r.userCrea')
            ->getQuery()
            ->getResult()
            ;
    }

    public function getDelationsForYear($year, $user) {
        $startDate = (new DateTime())
            ->setDate($year, 1, 1)
            ->setTime(0,0,0)
            ->format('Y-m-d H:i:s');
        $endDate = (new DateTime())
            ->setDate($year, 12, 31)
            ->setTime(23,59,59)
            ->format('Y-m-d H:i:s');
        $qb = $this->createQueryBuilder('r');
        return $qb->select("count(r.swearword) as total, MONTH(r.dateCrea) as mois")
            ->where('r.dateCrea >= :start_month')
            ->andWhere('r.dateCrea <= :end_month')
            ->andWhere('r.valid = true')
            ->setParameter(':start_month', $startDate)
            ->setParameter(':end_month', $endDate)
            ->andWhere("r.userCrea = :user")
            ->setParameter("user", $user)
            ->groupBy('mois')
            ->getQuery()
            ->getResult()
            ;
    }

    public function getReportsForYear($year, $user) {
        $startDate = (new DateTime())
            ->setDate($year, 1, 1)
            ->setTime(0,0,0)
            ->format('Y-m-d H:i:s');
        $endDate = (new DateTime())
            ->setDate($year, 12, 31)
            ->setTime(23,59,59)
            ->format('Y-m-d H:i:s');
        $qb = $this->createQueryBuilder('r');
        return $qb->select("count(r.swearword) as total, MONTH(r.dateCrea) as mois")
            ->where('r.dateCrea >= :start_month')
            ->andWhere('r.dateCrea <= :end_month')
            ->andWhere('r.valid = true')
            ->setParameter(':start_month', $startDate)
            ->setParameter(':end_month', $endDate)
            ->andWhere("r.user = :user")
            ->setParameter("user", $user)
            ->groupBy('mois')
            ->getQuery()
            ->getResult()
            ;
    }

    public function getDelationsForMonth($month, $user) {
        $year = intval((new \Datetime())->format('Y'));
        $startDate = (new DateTime())
            ->setDate($year, $month, 1)
            ->setTime(0,0,0)
            ->format('Y-m-d H:i:s');
        $endDate = (new DateTime())
            ->setDate($year, $month, $this->getEndMonth($month, $year))
            ->setTime(23,59,59)
            ->format('Y-m-d H:i:s');
        $qb = $this->createQueryBuilder('r');
        return $qb->select("count(r.swearword) as total")
            ->where('r.dateCrea >= :start_month')
            ->andWhere('r.dateCrea <= :end_month')
            ->andWhere('r.valid = true')
            ->setParameter(':start_month', $startDate)
            ->setParameter(':end_month', $endDate)
            ->andWhere("r.userCrea = :user")
            ->setParameter("user", $user)
            ->getQuery()
            ->getResult()
            ;
    }

    public function getReportsForMonth($month, $user) {
        $year = intval((new \Datetime())->format('Y'));
        $startDate = (new DateTime())
            ->setDate($year, $month, 1)
            ->setTime(0,0,0)
            ->format('Y-m-d H:i:s');
        $endDate = (new DateTime())
            ->setDate($year, $month, $this->getEndMonth($month, $year))
            ->setTime(23,59,59)
            ->format('Y-m-d H:i:s');
        $qb = $this->createQueryBuilder('r');
        return $qb->select("count(r.swearword) as total")
            ->where('r.dateCrea >= :start_month')
            ->andWhere('r.dateCrea <= :end_month')
            ->andWhere('r.valid = true')
            ->setParameter(':start_month', $startDate)
            ->setParameter(':end_month', $endDate)
            ->andWhere("r.user = :user")
            ->setParameter("user", $user)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findYears() {
        $qb = $this->createQueryBuilder('r');
        return $qb->select("distinct YEAR(r.dateCrea) as year")
            ->andWhere('r.valid = true')
            ->orderBy("year")
            ->getQuery()
            ->getResult()
            ;
    }

    public function getCagnotte() {
        $qb = $this->createQueryBuilder('r');
        return $qb->select("sum(r.price) as total")
            ->andWhere($qb->expr()->eq('r.valid', 'true'))
            ->andWhere($qb->expr()->eq('r.paid', 'true'))
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function getTotalForUser($user) {
        $qb = $this->createQueryBuilder('r');
        return $qb->select("sum(r.price) as total")
            ->where($qb->expr()->eq('r.user', ':user'))
            ->andWhere($qb->expr()->eq('r.paid', 'false'))
            ->andWhere($qb->expr()->eq('r.valid', 'true'))
            ->setParameter('user', $user)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function getCostForUser($user) {
        $qb = $this->createQueryBuilder('r');
        return $qb->select("sum(r.price) as total")
            ->where($qb->expr()->eq('r.user', ':user'))
            ->andWhere($qb->expr()->eq('r.valid', 'true'))
            ->setParameter('user', $user)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function getTotalUnpaid() {
        $qb = $this->createQueryBuilder('r');
        return $qb->select("sum(r.price) as total")
            ->andWhere($qb->expr()->eq('r.valid', 'true'))
            ->andWhere($qb->expr()->eq('r.paid', 'false'))
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function getThisMonth() {
        $month = intval((new DateTime('now'))->format('m'));
        $year = intval((new DateTime('now'))->format('Y'));
        $startDate = (new DateTime())
            ->setDate($year, $month, 1)
            ->setTime(0,0,0)
            ->format('Y-m-d H:i:s');
        $endDate = (new DateTime())
            ->setDate($year, $month, $this->getEndMonth($month, $year))
            ->setTime(23,59,59)
            ->format('Y-m-d H:i:s');
        $qb = $this->createQueryBuilder('r');
        return $qb->select('count(r) as total')
            ->where('r.dateCrea >= :start_month')
            ->andWhere('r.dateCrea <= :end_month')
            ->andWhere('r.valid = true')
            ->setParameter(':start_month', $startDate)
            ->setParameter(':end_month', $endDate)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function getMostRude() {
        $month = intval((new DateTime('now'))->format('m'));
        $year = intval((new DateTime('now'))->format('Y'));
        $startDate = (new DateTime())
            ->setDate($year, $month, 1)
            ->setTime(0,0,0)
            ->format('Y-m-d H:i:s');
        $endDate = (new DateTime())
            ->setDate($year, $month, $this->getEndMonth($month, $year))
            ->setTime(23,59,59)
            ->format('Y-m-d H:i:s');

        $qb = $this->createQueryBuilder('r');
        return $qb->select("count(r.swearword) as swearwords, concat(u.firstname, ' ', u.name) as user, u.surname as surname")
            ->leftJoin('r.user', 'u', Join::WITH, $qb->expr()->eq('u', 'r.user'))
            ->where($qb->expr()->eq('r.valid', 'true'))
            ->andWhere('r.dateCrea >= :start_month')
            ->andWhere('r.dateCrea <= :end_month')
            ->setParameter(':start_month', $startDate)
            ->setParameter(':end_month', $endDate)
            ->groupBy('r.user')
            ->orderBy('swearwords', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getMostCollabo() {
        $month = intval((new DateTime('now'))->format('m'));
        $year = intval((new DateTime('now'))->format('Y'));
        $startDate = (new DateTime())
            ->setDate($year, $month, 1)
            ->setTime(0,0,0)
            ->format('Y-m-d H:i:s');
        $endDate = (new DateTime())
            ->setDate($year, $month, $this->getEndMonth($month, $year))
            ->setTime(23,59,59)
            ->format('Y-m-d H:i:s');

        $qb = $this->createQueryBuilder('r');
        return $qb->select("count(r.swearword) as swearwords, concat(u.firstname, ' ', u.name) as user, u.surname as surname")
            ->leftJoin('r.userCrea', 'u', Join::WITH, $qb->expr()->eq('u', 'r.userCrea'))
            ->where($qb->expr()->eq('r.valid', 'true'))
            ->andWhere('r.dateCrea >= :start_month')
            ->andWhere('r.dateCrea <= :end_month')
            ->setParameter(':start_month', $startDate)
            ->setParameter(':end_month', $endDate)
            ->groupBy('r.userCrea')
            ->orderBy('swearwords', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }


    public function getByMonth($month, $year) {
        $qb = $this->createQueryBuilder('r');
        return $qb->select('count(r) as total')
            ->where($qb->expr()->gte('r.dateCrea', ':start_month'))
            ->andWhere($qb->expr()->gte('r.dateCrea', ':end_month'))
            ->andWhere($qb->expr()->eq('r.valid', 'true'))
            ->setParameter(':start_month', (new DateTime())->setDate($year, $month, 1))
            ->setParameter(':end_month',
                (new DateTime())->setDate($year, $month, $this->getEndMonth($month, $year)))
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function findPayments() {
        $qb = $this->createQueryBuilder('r');
        return $qb->select("concat(u.firstname,' ',u.name) as user, sum(r.price) as price, count(r.swearword) as nbSwearwords, u.id as userId ")
            ->leftJoin('r.user', 'u', Join::WITH, $qb->expr()->eq('u', 'r.user'))
            ->andWhere($qb->expr()->eq('r.paid', 'false'))
            ->andWhere($qb->expr()->eq('r.valid', 'true'))
            ->groupBy('user, userId')
            ->getQuery()
            ->getResult()
            ;
    }

    private function getEndMonth($month, $year) {
        if (in_array($month, [1, 3, 5, 7, 8, 10, 12])) {
            return '31';
        } else if (in_array($month, [4, 6, 9, 11])) {
            return '30';
        } else if ($month == 2) {
            if ($year % 4 == 0  && ($year % 100 != 0 || $year % 400 == 0)) {
                return '29';
            } else {
                return '28';
            }
        } else {
            return new Error("Le numéro du mois entré n'existe pas.");
        }
    }

}