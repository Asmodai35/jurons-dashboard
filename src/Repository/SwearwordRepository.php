<?php

namespace App\Repository;

use App\Entity\Swearword;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Swearword|null find($id, $lockMode = null, $lockVersion = null)
 * @method Swearword|null findOneBy(array $criteria, array $orderBy = null)
 * @method Swearword[]    findAll()
 * @method Swearword[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SwearwordRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Swearword::class);
    }

    public function labelLikes(string $label) {
        return $this->createQueryBuilder('s')
            ->andWhere('s.label LIKE :val')
            ->setParameter('val', $label)
            ->getQuery()
            ->getResult()
            ;
    }
 
    // /**
    //  * @return Swearword[] Returns an array of Swearword objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Swearword
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
