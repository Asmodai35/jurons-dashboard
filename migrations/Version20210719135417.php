<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210719135417 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE balance CHANGE credit credit NUMERIC(10, 2) DEFAULT NULL, CHANGE debit debit NUMERIC(10, 2) DEFAULT NULL');
        $this->addSql('ALTER TABLE report CHANGE user_id user_id INT DEFAULT NULL, CHANGE swearword_id swearword_id INT DEFAULT NULL, CHANGE user_crea user_crea INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD surname VARCHAR(255) DEFAULT NULL, CHANGE roles roles JSON NOT NULL, CHANGE name name VARCHAR(100) DEFAULT NULL, CHANGE firstname firstname VARCHAR(100) DEFAULT NULL, CHANGE birthday birthday DATETIME DEFAULT NULL, CHANGE ausy_arrival_date ausy_arrival_date DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE balance CHANGE credit credit NUMERIC(10, 2) DEFAULT \'NULL\', CHANGE debit debit NUMERIC(10, 2) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE report CHANGE user_id user_id INT DEFAULT NULL, CHANGE swearword_id swearword_id INT DEFAULT NULL, CHANGE user_crea user_crea INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user DROP surname, CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`, CHANGE name name VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE firstname firstname VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE birthday birthday DATETIME DEFAULT \'NULL\', CHANGE ausy_arrival_date ausy_arrival_date DATETIME DEFAULT \'NULL\'');
    }
}
