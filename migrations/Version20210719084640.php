<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210719084640 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE balance (id INT AUTO_INCREMENT NOT NULL, credit NUMERIC(10, 2) DEFAULT NULL, debit NUMERIC(10, 2) DEFAULT NULL, initial_amount NUMERIC(10, 2) NOT NULL, rest_amount NUMERIC(10, 2) NOT NULL, label VARCHAR(255) NOT NULL, created_at VARCHAR(255) NOT NULL, updated_at VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE report (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, swearword_id INT DEFAULT NULL, user_crea INT DEFAULT NULL, dateCrea DATETIME NOT NULL, valid TINYINT(1) NOT NULL, paid TINYINT(1) NOT NULL, price NUMERIC(10, 2) NOT NULL, INDEX IDX_C42F7784A76ED395 (user_id), INDEX IDX_C42F7784138082D6 (swearword_id), INDEX IDX_C42F778471CB92A (user_crea), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE swearword (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, created_at VARCHAR(255) NOT NULL, updated_at VARCHAR(255) NOT NULL, price NUMERIC(10, 2) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, name VARCHAR(100) DEFAULT NULL, firstname VARCHAR(100) DEFAULT NULL, birthday DATETIME DEFAULT NULL, joining_date DATETIME NOT NULL, ausy_arrival_date DATETIME DEFAULT NULL, created_at VARCHAR(255) NOT NULL, updated_at VARCHAR(255) NOT NULL, active TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F7784A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F7784138082D6 FOREIGN KEY (swearword_id) REFERENCES swearword (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F778471CB92A FOREIGN KEY (user_crea) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F7784138082D6');
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F7784A76ED395');
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F778471CB92A');
        $this->addSql('DROP TABLE balance');
        $this->addSql('DROP TABLE report');
        $this->addSql('DROP TABLE swearword');
        $this->addSql('DROP TABLE user');
    }
}
